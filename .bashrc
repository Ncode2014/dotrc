#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'
