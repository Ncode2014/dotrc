#!/bin/bash

# reference from : https://kbroman.org/github_tutorial/pages/first_time.html
echo "starting setup git configurations"
git config --global user.name "$1"
git config --global user.email "$2"
git config --global color.ui true
git config --global core.editor "$3"
sleep 5
echo "setup ssh"
ssh-keygen -t rsa -C "$2"
sleep 1
cat ~/.ssh/id_rsa.pub | pbcopy
