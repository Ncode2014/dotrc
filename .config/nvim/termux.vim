call plug#begin('~/vim/plugged')

" Themes Neovim
Plug 'rebelot/kanagawa.nvim'

" lsp stuff
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'j-hui/fidget.nvim'
Plug 'onsails/lspkind-nvim'
Plug 'kosayoda/nvim-lightbulb'
Plug 'weilbith/nvim-code-action-menu', {'do': 'CodeActionMenu'}

" auto complete
Plug 'kosayoda/nvim-lightbulb'
Plug 'dense-analysis/ale'

" for webdev stuff
Plug 'ap/vim-css-color'
Plug 'prettier/vim-prettier', {
            \ 'for': ['javascript', 'css', 'json', 'graphql', 'markdown', 'yaml', 'html'] }

" nerdtree
Plug 'preservim/nerdtree'

" moar feature
Plug 'max-0406/autoclose.nvim' " for make simple autoclose

" coq nvim
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}

" 9000+ Snippets
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}

" dim vim
Plug 'folke/twilight.nvim'
Plug 'folke/zen-mode.nvim'

" lsp color
Plug 'folke/lsp-colors.nvim'

" discord vim
Plug 'andweeb/presence.nvim'

" quick fix
Plug 'https://gitlab.com/yorickpeterse/nvim-pqf.git'

" spell checker
Plug 'lewis6991/spellsitter.nvim'

" cursor line
Plug 'yamatsum/nvim-cursorline'

" for markdown
Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
Plug 'godlygeek/tabular', { 'for': 'markdown' }
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }

" indent stuff
Plug 'lukas-reineke/indent-blankline.nvim'

" smooth scrolling
Plug 'karb94/neoscroll.nvim'

" for align stuff
Plug 'junegunn/vim-easy-align'

" window stuff
Plug 'luukvbaal/stabilize.nvim'

" readability
Plug 'nacro90/numb.nvim'

" for good git stuff
Plug 'tpope/vim-fugitive'

" treesitter stuff
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'SmiteshP/nvim-gps'
Plug 'm-demare/hlargs.nvim'


" status line
Plug 'ojroques/nvim-hardline'

" save life
Plug 'Pocco81/AutoSave.nvim'

" formatting code
Plug 'sbdchd/neoformat'

" python stuff
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'nvie/vim-flake8'

" a solid package code
Plug 'sheerun/vim-polyglot'

" markdown preview
Plug 'ellisonleao/glow.nvim'

" coc nvim
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'lukas-reineke/indent-blankline.nvim'
call plug#end()

" setting coc
set encoding=utf-8
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300

" lightbulb
autocmd CursorHold,CursorHoldI * lua require'nvim-lightbulb'.update_lightbulb()

lua << EOF
-- Showing defaults
require'nvim-lightbulb'.update_lightbulb {
    -- LSP client names to ignore
    -- Example: {"sumneko_lua", "null-ls"}
    ignore = {},
    sign = {
        enabled = true,
        -- Priority of the gutter sign
        priority = 10,
        },
    float = {
        enabled = false,
        -- Text to show in the popup float
        text = "💡",
        -- Available keys for window options:
        -- - height     of floating window
        -- - width      of floating window
        -- - wrap_at    character to wrap at for computing height
        -- - max_width  maximal width of floating window
        -- - max_height maximal height of floating window
        -- - pad_left   number of columns to pad contents at left
        -- - pad_right  number of columns to pad contents at right
        -- - pad_top    number of lines to pad contents at top
        -- - pad_bottom number of lines to pad contents at bottom
        -- - offset_x   x-axis offset of the floating window
        -- - offset_y   y-axis offset of the floating window
        -- - anchor     corner of float to place at the cursor (NW, NE, SW, SE)
        -- - winblend   transparency of the window (0-100)
        win_opts = {},
        },
    virtual_text = {
        enabled = false,
        -- Text to show at virtual text
        text = "💡",
        -- highlight mode to use for virtual text (replace, combine, blend), see :help nvim_buf_set_extmark() for reference
        hl_mode = "replace",
        },
    status_text = {
        enabled = false,
        -- Text to provide when code actions are available
        text = "💡",
        -- Text to provide when no actions are available
        text_unavailable = ""
        }
    }
EOF

" lspkind-nvim
lua << EOF
require('lspkind').init({
-- DEPRECATED (use mode instead): enables text annotations
--
-- default: true
-- with_text = true,

-- defines how annotations are shown
-- default: symbol
-- options: 'text', 'text_symbol', 'symbol_text', 'symbol'
mode = 'symbol_text',

-- default symbol map
-- can be either 'default' (requires nerd-fonts font) or
-- 'codicons' for codicon preset (requires vscode-codicons font)
--
-- default: 'default'
preset = 'codicons',

-- override preset symbols
--
-- default: {}
symbol_map = {
    Text = "",
    Method = "",
    Function = "",
    Constructor = "",
    Field = "ﰠ",
    Variable = "",
    Class = "ﴯ",
    Interface = "",
    Module = "",
    Property = "ﰠ",
    Unit = "塞",
    Value = "",
    Enum = "",
    Keyword = "",
    Snippet = "",
    Color = "",
    File = "",
    Reference = "",
    Folder = "",
    EnumMember = "",
    Constant = "",
    Struct = "פּ",
    Event = "",
    Operator = "",
    TypeParameter = ""
    },
})
EOF

" for glow
let g:glow_border = "rounded"

" kinda random configuration
syntax enable
set clipboard=unnamedplus
set expandtab
set ignorecase
set smartcase

" mappings vim-easy-align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" discord brrrr
let g:presence_auto_update = 1
let g:presence_neovim_image_text = "some kinda editor"
let g:presence_main_image = "neovim"
let g:presence_debounce_timeout = 10
let g:presence_enable_line_number = 0
let g:presence_button = 1

" rich presence text option
let g:presence_editing_text = "Sedang ngedit %s"
let g:presence_file_explorer_text = "Mencari %s"
let g:presence_git_commit_text = "komit sebuah perubahan"
let g:presence_plugin_manager_text = "Mengatur Plugins"
let g:presence_reading_text = "Membaca Code"
let g:presence_workspace_text = "Sedang Bekerja %s"
let g:presence_line_number_text = "Baris %s dari %s"

" title
let &titlestring = "Neovim"
set title

" save life
lua << EOF
local autosave = require("autosave")

autosave.setup(
{
    enabled = true,
    execution_message = "AutoSave: saved at " .. vim.fn.strftime("%H:%M:%S"),
    events = {"InsertLeave", "TextChanged"},
    conditions = {
        exists = true,
        filename_is_not = {},
        filetype_is_not = {},
        modifiable = true
        },
    write_all_buffers = false,
    on_off_commands = true,
    clean_command_line_interval = 0,
    debounce_delay = 135
}
)
EOF

" readability
lua << EOF
require('numb').setup({
show_cursorline = true,
number_only = false,
})
EOF

" for indent stuff
let g:vim.opt.listchars:append("eol:\")
let g:vim.opt.listchars:append("space:*")

lua << EOF
require("indent_blankline").setup {
    show_end_of_line = true,
    space_char_blankline = " ",
    }
EOF

" window
lua require("stabilize").setup()

" tresitter plugin stuff
lua require("nvim-gps").setup()
lua require('hlargs').setup()

" pqf stuff
lua require('pqf').setup()

" vim markdown stuff
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:tex_conceal = ""
let g:vim_markdown_math = 1

" autostart nerdtree
autocmd VimEnter * NERDTree

" for color stuff
lua require("twilight").setup()
lua require("zen-mode").setup()

" status line
lua require('hardline').setup()

" for smooth scrolling
lua require('neoscroll').setup()

" spellchecker setup
lua require('spellsitter').setup()

" base themes configurations
colorscheme kanagawa

" tab and space
set shiftwidth=4
set tabstop=4
set expandtab
set autoindent
set smartindent
set wrap
set linebreak
" note trailing space at end of next line
set showbreak=>\ \ \

function! StripTrailingWhitespace()
    normal mZ
    let l:chars = col("$")
    %s/\s\+$//e
    if (line("'Z") != line(".")) || (l:chars != col("$"))
        echo "Trailing whitespace stripped\n"
    endif
    normal `Z
endfunction

autocmd BufWritePre * call StripTrailingWhitespace()

" splits windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" autoindent
autocmd FileType html setlocal shiftwidth=2 tabstop=2 textwidth=120
autocmd FileType css setlocal shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType json setlocal shiftwidth=2 tabstop=2
autocmd FileType markdown setlocal shiftwidth=2 tabstop=2

" 24 rgb
if (empty($TMUX))
    if (has("nvim"))
        "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
        let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    endif
    "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
    "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
    " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
    if (has("termguicolors"))
        set termguicolors
    endif
endif

execute "set t_8f=\e[38;2;%lu;%lu;%lum"
execute "set t_8b=\e[48;2;%lu;%lu;%lum"

" some plugin requires this
set nocompatible

" set neovim for numbers
set number
set relativenumber
